package com.thymeleafhomework.controller;

import com.thymeleafhomework.model.Article;
import com.thymeleafhomework.repository.ArticleRepository;
import com.thymeleafhomework.validator.IsAdmin;
import com.thymeleafhomework.validator.IsEditor;
import com.thymeleafhomework.validator.IsReviewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ArticleController {
    @Autowired
    private ArticleRepository articleRepository;

    @GetMapping("/admin/article")
    @IsEditor
    @IsAdmin
    public String manageArticle(@ModelAttribute @Valid Article article, BindingResult result){

        return "/admin/article/article-management";
    }

    @PostMapping("/admin/article")
    @IsEditor
    @IsAdmin
    public String addUser(@ModelAttribute @Valid Article article, BindingResult result) {

        if (result.hasErrors()) {
            return "/admin/article/article-management";
        };
        articleRepository.save(article);

        return "redirect:/admin/review-articles";
    }

    @GetMapping("/admin/review-articles")
    @IsReviewer
    @IsAdmin
    public String viewArticle(ModelMap modelMap){
        List<Article> articles = (List<Article>) articleRepository.findAll();

        modelMap.addAttribute("articles", articles);

        return "/admin/article/article-review";
    }
}
