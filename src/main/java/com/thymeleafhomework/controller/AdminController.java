package com.thymeleafhomework.controller;

import com.thymeleafhomework.validator.IsAdmin;
import com.thymeleafhomework.validator.IsEditor;
import com.thymeleafhomework.validator.IsReviewer;
import com.thymeleafhomework.validator.IsUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class AdminController {

    @GetMapping("/admin")
    @IsUser
    @IsReviewer
    @IsEditor
    @IsAdmin
    public String getAdmin(){
        return "/admin/admin";
    }

    @GetMapping("/admin/login")
    public String getLoginPage(){
        return "/admin/login";
    }

    @GetMapping("/admin/logout")
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "/admin/login";
    }

    @GetMapping("/admin/dashboard")
    @IsAdmin
    public String getDashboard(){
        return "/admin/dashboard-statistic";
    }

    @GetMapping("/login-error")
    public String getLoginError(){
        return "/error/403";
    }
}
