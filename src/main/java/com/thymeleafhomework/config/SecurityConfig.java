package com.thymeleafhomework.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    @Override
    public UserDetailsService userDetailsService() {

        UserDetails theUser = User.withUsername("dara")
                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
                .password("dara123").roles("USER").build();

        UserDetails theEditor = User.withUsername("kanha")
                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
                .password("kanha123").roles("EDITOR").build();

        UserDetails theReviewer = User.withUsername("reksmey")
                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
                .password("reksmey123").roles("REVIEWER").build();

        UserDetails theAdmin = User.withUsername("makara")
                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
                .password("makara123").roles("ADMIN").build();


        InMemoryUserDetailsManager userDetailsManager = new InMemoryUserDetailsManager();

        userDetailsManager.createUser(theUser);
        userDetailsManager.createUser(theEditor);
        userDetailsManager.createUser(theReviewer);
        userDetailsManager.createUser(theAdmin);

        return userDetailsManager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .formLogin()
//                .loginPage("/admin/login")
//                .failureUrl("/login-error")
//                .defaultSuccessUrl("/admin")
//                .permitAll()
//                .and()
//                .logout()
//                .logoutUrl("/admin/logout")
//                .logoutSuccessUrl("/admin/login")
//                .permitAll()
//                .and()
//                .exceptionHandling().accessDeniedPage("/error/403")
//                .and()
//                .csrf().disable()
//                .authorizeRequests()
//                .antMatchers("/admin").hasAnyRole("USER", "EDITOR", "REVIEWER", "ADMIN")
//                .antMatchers("/admin/login").hasAnyRole("USER", "EDITOR", "REVIEWER", "ADMIN")
//                .antMatchers("/admin/logout").hasAnyRole("USER", "EDITOR", "REVIEWER", "ADMIN")
//                .antMatchers("/admin/dashboard").hasRole("ADMIN")
//                .antMatchers("/admin/article").hasAnyRole("EDITOR", "ADMIN")
//                .antMatchers("/admin/review-articles").hasAnyRole( "REVIEWER", "ADMIN")
//                .antMatchers("/404").hasAnyRole("USER", "EDITOR", "REVIEWER", "ADMIN")
//                .antMatchers("/403").hasAnyRole("USER", "EDITOR", "REVIEWER", "ADMIN")
//                .antMatchers("/500").hasAnyRole("USER", "EDITOR", "REVIEWER", "ADMIN")
//                .anyRequest().authenticated()
//                .and()
//                .httpBasic()
//                .and()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/admin/login")
                .successHandler(authenticationSuccessHandler())
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/admin/logout")
                .logoutSuccessUrl("/admin/login")
                .permitAll()
                .and().csrf().disable();
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new AuthenticationSuccessHandler();
    }
}
