package com.thymeleafhomework.repository;

import com.thymeleafhomework.model.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends CrudRepository<Article,Integer> {
}
